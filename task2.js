const express = require('express');
const {Vigenere} = require("caesar-salad");
const app = express();
const port = 8000;

app.get('/encode/:name', (req, res) => {
    const Vigenere = require('caesar-salad').Vigenere;
    const encodedMessage = Vigenere.Cipher('password').crypt(req.params.name);
    res.send(`<h1>Encode message:</h1> <p>${encodedMessage}</p>\n`);
});

app.get('/decode/:name', (req, res) => {
    const Vigenere = require('caesar-salad').Vigenere;
    const encodedMessage = Vigenere.Decipher('password').crypt(req.params.name);
    res.send(`<h1>Decode message:</h1> <p>${encodedMessage}</p>\n`);
});

app.listen(port, () => {
    console.log('We are live on ' + port);
});

