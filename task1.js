const express = require('express');
const app = express();
const port = 8000;

app.get('/:name', (req, res) => {
    res.send(`<h1>Returned message:</h1> <p>${req.params.name}</p>\n`);
});

app.listen(port, () => {
    console.log('We are live on ' + port);
});
